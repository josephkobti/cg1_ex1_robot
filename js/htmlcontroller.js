var HtmlController = function (sceneController) {
    this.sceneController = sceneController;
    this.mouseStart = new THREE.Vector2();
    this.previousMousePosition = {
        x: 0,
        y: 0
    };
    this.isDragging = false;
    this.selectionMode = true;
};

HtmlController.prototype.setup = function () {
    //bind? --> https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
    window.addEventListener('resize', this.onWindowResize.bind(this), false);
    document.addEventListener("keydown", this.onDocumentKeyDown.bind(this), false);
    document.addEventListener("mousedown", this.onDocumentMouseDown.bind(this), false);
    document.addEventListener("mousemove", this.onDocumentMouseMove.bind(this), false);
    document.addEventListener("mouseup", this.onDocumentMouseUp.bind(this), false);
};

//event handler that is called when the window is resized
HtmlController.prototype.onWindowResize = function () {
    this.sceneController.camera.aspect = window.innerWidth / window.innerHeight;
    this.sceneController.camera.updateProjectionMatrix();

    this.sceneController.renderer.setSize(window.innerWidth, window.innerHeight);
    this.sceneController.render();
};

HtmlController.prototype.onDocumentMouseDown = function (event) {
    if (this.selectionMode) {
        this.sceneController.handleMouseClick(event);
        return;
    }
    this.isDragging = true;
};

HtmlController.prototype.onDocumentMouseMove = function (event) {
    if (this.selectionMode)
        return;
    var deltaMove = {
        x: event.offsetX - this.previousMousePosition.x,
        y: event.offsetY - this.previousMousePosition.y
    };

    if (selectedId == 0) {
        return;
    }

    if (this.isDragging) {
        this.sceneController.handleMouseMove(deltaMove.x, deltaMove.y);
    }

    this.previousMousePosition = {
        x: event.offsetX,
        y: event.offsetY
    };
};

HtmlController.prototype.onDocumentMouseUp = function (event) {
    this.isDragging = false;
};


HtmlController.prototype.onDocumentKeyDown = function (event) {
    var keyCode = event.key;

    switch (keyCode) {
        case "r":
            this.selectionMode = true;
            this.sceneController.controls.reset();
            this.sceneController.reset();
            break;
        case "ArrowUp":
            this.sceneController.rotateNode(THREE.Vector3.XAxis, 2);
            break;
        case "ArrowDown":
            this.sceneController.rotateNode(THREE.Vector3.XAxis, -2);
            break;
        case "ArrowRight":
            this.sceneController.rotateNode(THREE.Vector3.YAxis, 2);
            break;
        case "ArrowLeft":
            this.sceneController.rotateNode(THREE.Vector3.YAxis, -2);
            break;
        case "h":
            toggleVisibility(document.getElementById("instructions"));
            break;
        case "s":
            this.sceneController.selectChild();
            break;
        case "w":
            this.sceneController.selectParent();
            break;
        case "d":
            this.sceneController.selectNextSibling();
            break;
        case "a":
            this.sceneController.selectPreviousSibling();
            break;
        case "c":
            this.sceneController.toggleAxisVisibility();
            break;
        case "e":
            if (this.selectionMode) {
                if (selectedId == 0) {
                    window.alert('please select first a body part');
                    return;
                }
                this.selectionMode = false;
                this.sceneController.switchControlsMode(false);
                window.alert("you can now rotate the selected body part");
            } else {
                this.selectionMode = true;
                this.sceneController.switchControlsMode(true);
                window.alert("you can now select a body part");
            }

            break;
    }
    this.sceneController.render();
};