var SceneController = function (document) {
  targetList = [];
  selectedId = 0;
  oldColor = '';
  this.scene = new THREE.Scene();
  scene2 = new THREE.Scene();
  this.renderer = new THREE.WebGLRenderer({
    antialias: true, // to enable anti-alias and get smoother output
    preserveDrawingBuffer: true // to allow screenshot
  });

  raycaster = new THREE.Raycaster();
  dir = new THREE.Vector3();
  vector = new THREE.Vector3();
};

SceneController.prototype.setup = function () {
  // https://threejs.org/docs/#api/renderers/WebGLRenderer
  this.renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(this.renderer.domElement);
  this.setupCamera();
  this.setupControls();
  this.setupLight();
  this.setupGeometry();
  this.render();
  this.animate();
};

SceneController.prototype.setupCamera = function () {
  var VIEW_ANGLE = 70;
  var ASPECT_RATIO = window.innerWidth / window.innerHeight;
  var NEAR_PLANE = 0.01;
  var FAR_PLANE = 10;

  // https://threejs.org/docs/#api/cameras/PerspectiveCamera
  this.camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT_RATIO, NEAR_PLANE, FAR_PLANE);
  this.camera.position.z = 2;
  this.camera.lookAt(this.scene.position);
};

SceneController.prototype.setupControls = function () {
  // https://threejs.org/examples/misc_controls_orbit.html
  this.controls = new THREE.OrbitControls(this.camera);
  this.controls.enableDamping = true;
  this.controls.enableZoom = true;
  this.controls.enableKeys = false;
  this.controls.enabled = true;
  //bind? --> https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
  this.controls.addEventListener('change', this.render.bind(this));
};

SceneController.prototype.setupGeometry = function () {
  this.robot = new Robot();
  this.scene.add(this.robot.buildRobot());
};

SceneController.prototype.setupLight = function () {
  // https://threejs.org/docs/#api/lights/PointLight
  var light = new THREE.PointLight(0xffffcc, 1, 100);
  light.position.set(10, 30, 15);
  this.scene.add(light);

  var light2 = new THREE.PointLight(0xffffcc, 1, 100);
  light2.position.set(10, -30, -15);
  this.scene.add(light2);

  this.scene.add(new THREE.AmbientLight(0x999999));
};

SceneController.prototype.render = function () {

  this.renderer.render(this.scene, this.camera);
};

SceneController.prototype.animate = function () {
  //bind? --> https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
  requestAnimationFrame(this.animate.bind(this));
  this.controls.update();
};

SceneController.prototype.reset = function () {
  this.robot.reset();
};

SceneController.prototype.toggleSelection = function () {
  this.robot.toggleSelection();
};

SceneController.prototype.selectNextSibling = function (forward) {
  this.robot.selectNextSibling(forward);
  this.renderer.render(this.scene, this.camera);

};
SceneController.prototype.selectPreviousSibling = function (forward) {
  this.robot.selectPreviousSibling(forward);
  this.renderer.render(this.scene, this.camera);

};

SceneController.prototype.selectParent = function (forward) {
  this.robot.selectParent(forward);
  this.renderer.render(this.scene, this.camera);

};

SceneController.prototype.selectChild = function (forward) {
  this.robot.selectChild(forward);
  this.renderer.render(this.scene, this.camera);

};

SceneController.prototype.toggleAxisVisibility = function () {
  this.robot.toggleAxis();
};

SceneController.prototype.rotateNode = function (axis, degree) {
  this.robot.rotateOnAxis(axis, degree);
};

SceneController.prototype.handleMouseClick = function (event) {
  vector.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
  vector.unproject(this.camera);
  raycaster.set(this.camera.position, vector.sub(this.camera.position).normalize());

  var intersects = raycaster.intersectObjects(targetList, true);
  if (intersects.length == 0)
    return;
  this.robot.toggleSelection(intersects[0].object.id);
  this.renderer.render(this.scene, this.camera);
};

SceneController.prototype.handleMouseMove = function (offsetX, offsetY) {
  var deltaRotationQuaternion = new THREE.Quaternion()
    .setFromEuler(new THREE.Euler(
      degToRad(offsetY * 0.5),
      degToRad(offsetX * 0.5),
      0,
      'XYZ'
    ));
  this.robot.quaternionRotate(deltaRotationQuaternion);
  this.renderer.render(this.scene, this.camera);
};

SceneController.prototype.switchControlsMode = function (mode) {
  this.controls.enabled = mode;
};