var Robot = function () {
  this.root = new THREE.Object3D();
};

Robot.prototype.buildRobot = function () {
  var front_torso_height = 0.3;
  var front_leg_height = 0.6;
  var middle_leg_height = 0.4;
  var bodyColor = "darkred";
  var legColor = "crimson";
  var secondLegColor = "darksalmon";

  var geometry = new THREE.SphereGeometry(0.2, 0.4, front_torso_height);
  var material = new THREE.MeshBasicMaterial({
    color: bodyColor
  });
  var torso = new THREE.Mesh(geometry, material);
  torso.name = "torso";
  torso.material.transparent = true;
  torso.material.opacity = 0.6;
  this.root.add(torso);
  targetList.push(torso);
  torso.parent.name = "mainJ";

  var jointTB = this.buildJoint(0, -0.2, "jointTB");
  torso.add(jointTB);

  /**********    torso_lower    **********/
  var geometryBackTorso = new THREE.SphereGeometry(
    0.2,
    0.8,
    front_torso_height
  );
  var materialBackTorso = new THREE.MeshBasicMaterial({
    color: bodyColor
  });
  var BackTorso = new THREE.Mesh(geometryBackTorso, materialBackTorso);
  targetList.push(BackTorso);
  BackTorso.position.y = -0.15;
  BackTorso.material.transparent = true;
  BackTorso.material.opacity = 0.6;
  BackTorso.name = "BackTorso";
  jointTB.add(BackTorso);
  /**********    Torso_joints    **********/
  var jointTF1 = this.buildJoint(0.11, front_torso_height / 2, "jointTF1");
  var jointTF2 = this.buildJoint(-0.11, front_torso_height / 2, "jointTF2");
  var jointTM1 = this.buildJoint(
    0.18,
    front_torso_height / 2 - 0.1,
    "jointTM1"
  );
  var jointTM2 = this.buildJoint(
    -0.18,
    front_torso_height / 2 - 0.1,
    "jointTM2"
  );
  var jointTM3 = this.buildJoint(
    0.15,
    front_torso_height / 2 - 0.25,
    "jointTM3"
  );
  var jointTM4 = this.buildJoint(
    -0.15,
    front_torso_height / 2 - 0.25,
    "jointTM4"
  );
  var jointTB1 = this.buildJoint(
    0.18,
    front_torso_height / 2 - 0.1,
    "jointTB1"
  );
  var jointTB2 = this.buildJoint(
    -0.18,
    front_torso_height / 2 - 0.1,
    "jointTB2"
  );

  torso.add(jointTF1);
  torso.add(jointTF2);
  torso.add(jointTM1);
  torso.add(jointTM2);
  torso.add(jointTM3);
  torso.add(jointTM4);
  BackTorso.add(jointTB1);
  BackTorso.add(jointTB2);

  /**********    FRONT    **********/
  /**********    FRONT_1    **********/
  var front_1_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "front_1_1"
  );
  var front_2_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "front_2_1"
  );

  front_1_1.position.y = front_leg_height / 2;
  front_2_1.position.y = front_leg_height / 2;

  jointTF1.add(front_1_1);
  jointTF2.add(front_2_1);

  var jointF1 = this.buildJoint(0.05, front_leg_height / 2 - 0.05, "jointF1");
  var jointF2 = this.buildJoint(-0.05, front_leg_height / 2 - 0.05, "jointF2");

  front_1_1.add(jointF1);
  front_2_1.add(jointF2);

  /**********    FRONT_2    **********/
  var front_1_2 = this.buildBodyPart(
    0.1,
    0.2,
    0.2,
    secondLegColor,
    "front_1_2"
  );
  var front_2_2 = this.buildBodyPart(
    0.1,
    0.2,
    0.2,
    secondLegColor,
    "front_2_2"
  );

  front_1_2.position.y = 0.1;
  front_2_2.position.y = 0.1;

  jointF1.add(front_1_2);
  jointF2.add(front_2_2);

  /**********    Middle_1 & middle_2    **********/
  /**********    Middle_1_1 & middle_2_1     **********/
  var middle_1_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "middle_1_1"
  );
  var middle_2_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "middle_2_1"
  );

  middle_2_1.name = "middle_2_1";

  middle_1_1.position.y = middle_leg_height / 2 + 0.1;
  middle_2_1.position.y = middle_leg_height / 2 + 0.1;

  jointTM1.add(middle_1_1);
  jointTM2.add(middle_2_1);

  var jointM1 = this.buildJoint(0.05, front_leg_height / 2 - 0.05, "jointM1");
  var jointM2 = this.buildJoint(-0.05, front_leg_height / 2 - 0.05, "jointM2");

  middle_1_1.add(jointM1);
  middle_2_1.add(jointM2);
  /**********    Middle_1_2 & Middle_2_2   **********/
  var middle_1_2 = this.buildBodyPart(
    0.1,
    0.2,
    0.2,
    secondLegColor,
    "middle_1_2"
  );
  var middle_2_2 = this.buildBodyPart(
    0.1,
    0.2,
    0.2,
    secondLegColor,
    "middle_2_2"
  );

  middle_1_2.position.x = 0;
  middle_2_2.position.x = 0;

  middle_1_2.position.y = 0.1;
  middle_2_2.position.y = 0.1;

  jointM1.add(middle_1_2);
  jointM2.add(middle_2_2);

  /**********    Middle_3 & Middle_4     **********/
  /**********    Middle_3_1 & Middle_4_1   **********/
  var middle_3_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "middle_3_1"
  );
  var middle_4_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "middle_4_1"
  );

  middle_3_1.position.y = middle_leg_height / 2 + 0.11;
  middle_4_1.position.y = middle_leg_height / 2 + 0.11;

  jointTM3.add(middle_3_1);
  jointTM4.add(middle_4_1);

  var jointM3 = this.buildJoint(0.05, front_leg_height / 2 - 0.05, "jointM3");
  var jointM4 = this.buildJoint(-0.05, front_leg_height / 2 - 0.05, "jointM4");

  middle_3_1.add(jointM3);
  middle_4_1.add(jointM4);
  /**********    Middle_3_2 & Middle 4_2     **********/
  var middle_3_2 = this.buildBodyPart(
    0.1,
    0.2,
    0.2,
    secondLegColor,
    "middle_3_2"
  );
  var middle_4_2 = this.buildBodyPart(
    0.1,
    0.2,
    0.2,
    secondLegColor,
    "middle_4_2"
  );

  middle_3_2.position.x = 0;
  middle_4_2.position.x = 0;

  middle_3_2.position.y = 0.1;
  middle_4_2.position.y = 0.1;

  jointM3.add(middle_3_2);
  jointM4.add(middle_4_2);

  /**********    Back Legs    **********/
  /**********    Back_1    **********/
  var Back_1_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "Back_1_1"
  );
  var Back_2_1 = this.buildBodyPart(
    0.1,
    front_torso_height * 2,
    0.2,
    legColor,
    "Back_2_1"
  );

  Back_1_1.position.y = middle_leg_height / 2 + 0.11;
  Back_2_1.position.y = middle_leg_height / 2 + 0.11;

  jointTB1.add(Back_1_1);
  jointTB2.add(Back_2_1);

  var jointB1 = this.buildJoint(0.05, front_leg_height / 2 - 0.05, "jointB1");
  var jointB2 = this.buildJoint(-0.05, front_leg_height / 2 - 0.05, "jointB2");

  Back_1_1.add(jointB1);
  Back_2_1.add(jointB2);

  /**********    Back_2    **********/
  var Back_1_2 = this.buildBodyPart(0.1, 0.2, 0.2, secondLegColor, "Back_1_2");
  var Back_2_2 = this.buildBodyPart(0.1, 0.2, 0.2, secondLegColor, "Back_2_2");

  Back_1_2.position.y = 0.1;
  Back_2_2.position.y = 0.1;

  jointB1.add(Back_1_2);
  jointB2.add(Back_2_2);

  /**********    Torso Finish    **********/
  this.reset();
  return this.root;
};

Robot.prototype.buildBodyPart = function (g1, g2, g3, CSScolor, name) {
  var geometry = new THREE.BoxGeometry(g1, g2, g3);
  var material = new THREE.MeshBasicMaterial({
    color: CSScolor
  });
  var bodyPart = new THREE.Mesh(geometry, material);
  bodyPart.name = name;
  bodyPart.material.transparent = true;
  bodyPart.material.opacity = 0.6;
  targetList.push(bodyPart);
  return bodyPart;
};

Robot.prototype.buildJoint = function (x, y, name) {
  var joint = new THREE.Object3D();
  joint.name = name;
  joint.position.x = x;
  joint.position.y = y;
  return joint;
};

Robot.prototype.reset = function () {
  var torsoJoints = [
    "jointTF1",
    "jointTF2",
    "jointTM1",
    "jointTM2",
    "jointTM3",
    "jointTM4",
    "jointTB1",
    "jointTB2"
  ];
  var torsoJointsAngles = [-15, 15, -40, 40, -110, 110, -135, 135];
  var smallJoints = [
    "jointF1",
    "jointF2",
    "jointM1",
    "jointM2",
    "jointM3",
    "jointM4",
    "jointB1",
    "jointB2",
    "mainJ",
    "jointTB"
  ];

  for (let i = 0; i < torsoJoints.length; i++) {
    let joint = this.root.getObjectByName(torsoJoints[i]);
    joint.rotation.z = 0;
    joint.rotation.x = 0;
    joint.rotation.y = 0;
    joint.rotateOnAxis(THREE.Vector3.ZAxis, degToRad(torsoJointsAngles[i]));
    oldAxis = joint.getObjectByName(joint.id + "axes");
    if (oldAxis) {
      joint.remove(oldAxis);
    }
  }
  for (let i = 0; i < smallJoints.length; i++) {
    let joint = this.root.getObjectByName(smallJoints[i]);
    joint.rotation.z = 0;
    joint.rotation.x = 0;
    joint.rotation.y = 0;
    oldAxis = joint.getObjectByName(joint.id + "axes");
    if (oldAxis) {
      joint.remove(oldAxis);
    }
  }
  this.root.getObjectByName("torso").rotation.z = 0;
  this.root.getObjectByName("torso").rotation.x = 0;
  this.root.getObjectByName("torso").rotation.y = 0;

  if (selectedId != 0) {
    this.root.getObjectById(selectedId).material.color.set(oldColor);
    oldColor = "";
    selectedId = 0;
  }
};

Robot.prototype.selectParent = function (forward) {
  if (selectedId == 0) {
    alert("select your first node first");
    return;
  }
  parent = this.root.getObjectById(selectedId).parent;
  if (parent.type == "Object3D") {
    parent = parent.parent;
  }
  if (parent.type == "Mesh") {
    this.toggleSelection(parent.id);
  }
};

Robot.prototype.selectChild = function (forward) {
  if (selectedId == 0) {
    var child = this.root.children[0];
    this.toggleSelection(child.id);
  } else {
    var children = this.root.getObjectById(selectedId).children;
    if (children.length == 0) {
      return;
    }
    child = children[0];
    if (child.type == "Object3D") {
      child = child.children[0];
    }
    this.toggleSelection(child.id);
  }
};

Robot.prototype.selectNextSibling = function (forward) {
  if (selectedId == 0 || selectedId == 14) {
    return;
  }
  var JointID = this.root.getObjectById(selectedId, true).parent.id;
  var parentID = this.root.getObjectById(JointID, true).parent.id;
  var allChildren = this.root.getObjectById(parentID, true).children;
  var siblingId = allChildren[0].children[0].id;
  var oldNodeFound = false;
  for (var i = 0; i < allChildren.length; i++) {
    if (allChildren[i].id == JointID) {
      oldNodeFound = true;
      continue;
    }
    if (oldNodeFound) {
      siblingId = allChildren[i].children[0].id;
      siblingJointId = allChildren[i].id;
      break;
    }
  }
  this.toggleSelection(siblingId);
};

Robot.prototype.selectPreviousSibling = function (forward) {
  if (selectedId == 0 || selectedId == 14) {
    return;
  }
  var JointID = this.root.getObjectById(selectedId, true).parent.id;
  var parentID = this.root.getObjectById(JointID, true).parent.id;
  var allChildren = this.root.getObjectById(parentID, true).children;
  var siblingId = allChildren[allChildren.length - 1].children[0].id;
  for (let i = 1; i < allChildren.length; i++) {
    if (allChildren[i].id == JointID) {
      siblingId = allChildren[i - 1].children[0].id;
      break;
    }
  }
  this.toggleSelection(siblingId);
};

Robot.prototype.toggleSelection = function (newID) {
  if (!this.root.getObjectById(newID).name) {
    return;
  }
  if (selectedId != 0) {
    this.root.getObjectById(selectedId).material.color.set(oldColor);
  }
  oldColor = this.root.getObjectById(newID).material.color.getStyle();
  this.root.getObjectById(newID).material.color.set(new THREE.Color("gold"));
  selectedId = newID;
};

Robot.prototype.rotateOnAxis = function (axis, degree) {
  if (selectedId == 0 || selectedId == 16) {
    return;
  }
  var parentJoint = this.root.getObjectById(selectedId, true).parent;
  parentJoint.rotateOnAxis(axis, degToRad(degree));
};

Robot.prototype.toggleAxis = function () {
  var torsoJoints = [
    "jointTF1",
    "jointTF2",
    "jointTM1",
    "jointTM2",
    "jointTM3",
    "jointTM4",
    "jointTB1",
    "jointTB2"
  ];
  var smallJoints = [
    "jointF1",
    "jointF2",
    "jointM1",
    "jointM2",
    "jointM3",
    "jointM4",
    "jointB1",
    "jointB2",
    "mainJ",
    "jointTB"
  ];

  for (let i = 0; i < torsoJoints.length; i++) {
    let joint = this.root.getObjectByName(torsoJoints[i]);
    oldAxis = joint.getObjectByName(joint.id + "axes");
    if (oldAxis) {
      joint.remove(oldAxis);
    } else {
      axisObject = buildAxes(0.2);
      axisObject.name = joint.id + "axes";
      joint.add(axisObject);
    }
  }
  for (let i = 0; i < smallJoints.length; i++) {
    let joint = this.root.getObjectByName(smallJoints[i]);
    oldAxis = joint.getObjectByName(joint.id + "axes");
    if (oldAxis) {
      joint.remove(oldAxis);
    } else {
      axisObject = buildAxes(0.2);
      axisObject.name = joint.id + "axes";
      joint.add(axisObject);
    }
  }
};

Robot.prototype.quaternionRotate = function (quatrion) {
  cube = this.root.getObjectById(selectedId).parent;
  cube.applyQuaternion(quatrion);
};